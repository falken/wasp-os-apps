# Rogue clock face

## REPL sim 
`while inotifywait -e modify rogue.py; do   killall python3;   nohup make sim 2>&1 & done`

## Deploy temp
`./tools/wasptool --exec rogue.py --eval "wasp.system.unregister(RogueApp) ; wasp.system.register(RogueApp())"`

## Deploy perm (as binary)

`./micropython/mpy-cross/mpy-cross -mno-unicode -march=armv7m  rogue.py && ./tools/wasptool --binary --upload rogue.mpy &&  ./tools/wasptool --reset`

### ...and test it
```
./tools/wasptool --console
```
```
from rogue import RogueApp
wasp.system.register(RogueApp())
```

### Auto start, one time only
Edit in the main WaspOS project ..../wasp-os/wasp/main.py to add
```
from rogue import RogueApp
wasp.system.register(RogueApp())
```
Then 
`./tools/wasptool --upload wasp/main.py`
And reboot 
`./tools/wasptool --reset`

The app will now appear on the main list of things when you swipe up.

## Deploy perm (as source)

Run
`./tools/wasptool --upload rogue.py`

This doesn't work beyond super small apps : https://github.com/wasp-os/wasp-os/issues/346

### One time only
Edit main.py to add
```
from rogue import RogueApp
wasp.system.register(RogueApp())
```
Then 
``./tools/wasptool --upload wasp/main.py`
And reboot 
`./tools/wasptool --reset`