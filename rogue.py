import wasp
import time

from micropython import const

class RogueApp():
    NAME = "Rogue"
    # 2-bit RLE, 45x47, generated from rogue.png, 487 bytes
    ICON = (
        b'\x02'
        b'-/'
        b'\x07\x1e\x0f\x1e\x0f\x1e\x0f\x1e\x0f\x1e\x0f\x0b@\xffC\x01'
        b'A\x80\x80\x81C\n\x0f\x08B\xc0\xeb\xc4A\xc7A\x07'
        b'\x0f\x06B\xc6A\xc7A\x01@TA\x05\x0f\x06\x80\xff'
        b'\x81\xc6\x84\xc0z\xc1@\xebD\x82A\x82\x03\x0f\x04\x83'
        b'C\x82\x80T\x81\x06\xc0\xff\xc4D\xc1\x02\x0f\x02\xc2B'
        b'\xc3\x81\x0c\xc1E\xc1\x01\x08\t\xc1C\xc1\x11\xc1D\xc1'
        b'\x10\xc1C@\x80A\xc1\x12\xc1\x80\xeb\x84\x0f\xc1\x84\x15'
        b'\xc1\x82\xc1\x0f\xc1\x83\xc1\x08\xc0H\xc1\x01@\xffC\t'
        b'C\x81A\rA\x80z\x81\xc0\xeb\xc1A\x07A\xc2A'
        b'@\x80A\xc2\x80\xff\x82\x08\x81\xc3\x0f\x81\x08\x81\xc2\x82'
        b'\xc1\x82\xc2\x81\x06\x81\xc3\x81\x15\xc0z\xc1\x84\x02\x81\x01'
        b'\x81@\xebB\x81\x07D\x14\x81B\x08\x84\x06\x81C\x81'
        b'\x13\x81A\x81\n\x81A\x81\x05\x81C\x81\x12\x83\x0b\x81'
        b'B\x05\x80H\x81C\xc0\xff\xc1\x12@zA\x80\xeb\x81'
        b'\xc1\x0b\xc1\x81\xc1\x06\xc1\x81\xc2\x11\xc1\x82\xc1\x0c\xc3\x06'
        b'\xc3\x11\xc0T\xc1@\xffC\x0bB\x81A\x05A\x82A'
        b'\nB\x07A\xc1\x0bA\x82A\x05A\x82\x80\x80\x81\x08'
        b'A\x81\xc0\xeb\xc2A\x05A\xc1A\x0cC\x05A\xc2A'
        b'\x08A\xc3A\x05A\xc2A\nB\x06A\xc3A\t\xc3'
        b'A\x06B@HA\t\x80\xff\x81\xc2\x81\x05\x81\xc3\x81'
        b'\t\x81\xc3\x07\x81\xc2\xc0T\xc1\x07\x81@\xebA\x81\x06'
        b'\x81C\xc1\t\x81C\x81\x06\x81B\x81\x80H\x81\xc0\xff'
        b'\xc2\x05\xc1\x07\xc1B\xc1\x0bD\x07\xc1@zA\xc2\x80'
        b'\xeb\x82\xc1\x0b\xc5\x0b\xc1\x83\n\xc1\x82\xc1\n\xc1\x83\xc1'
        b'\r\xc5\x16\xc0\x80\xc1\x83@\xffA\x0eA\x83A\x14A'
        b'\x83A\x0fA\x84A\x11B\x83\x80z\x81\x11A\xc0\xeb'
        b'\xc4A\x10A\xc4A\x12A\xc4A\x0cB\xc1A\xc2@'
        b'\x80A\x80\xff\x81\x14\x81\xc3\x82\xc1\x84\x03\x82\xc0z\xc1'
        b'@\xebC\x84\x16\x81\xc1\x81\x01G\x82F\xc1\x1c\x81G'
        b'\x81\x80\x80\x81E\xc0\xff\xc2\x1d\xc2E\xc2C\xc1#\xc5'
        b'\x01\xc1?\xb5'
    )

    def __init__(self):
        self._WIDTH  = const( wasp.watch.display.width )
        self._HEIGHT = const( wasp.watch.display.width )
        self._WIDTH_2  = const( int(self._WIDTH/2))
        self._HEIGHT_2 = const( int(self._HEIGHT/2) )

        self._C = wasp.system.theme('mid')
        self._BRIGHT = wasp.system.theme('bright')
        self._D = wasp.watch.drawable.darken( wasp.system.theme('contrast'),50 )

        self._hh = -1
        self._mm = -1

    def foreground(self):
        wasp.system.bar.clock = False

        draw = wasp.watch.drawable  
        draw.fill()

        self._draw(True)

        wasp.system.request_event(wasp.EventMask.TOUCH  )
        # units - ms
        wasp.system.request_tick(60*1000) 

    def sleep(self):
        return True

    def wake(self): 
        self._draw()

    def tick(self, ticks):
        self._draw()

    def touch(self,event):
        draw = wasp.watch.drawable

        now = wasp.system.bar.update()
        if not now:
            now = wasp.watch.rtc.get_localtime()

        h = now[3] % 12
        m = now[4]

        for theta in range(12):
            self._draw_h_at_in(draw,theta,self._BRIGHT)
            self._draw_m_at_in(draw,theta,self._BRIGHT)

        time.sleep_ms(250)

        for theta in range(12):
            self._draw_h_at_in(draw,theta,self._C)
            time.sleep_ms(50)
            self._draw_h_at_in(draw,theta,self._BRIGHT )
            self._draw_m_at_in(draw,theta,self._C)
            time.sleep_ms(50)
            self._draw_m_at_in(draw,theta,self._BRIGHT)
            time.sleep_ms(50)

        time.sleep_ms(250)

        for theta in range(12):
            self._draw_h_at_in(draw,theta,self._C )
            self._draw_m_at_in(draw,theta,self._C)

        self._draw(True)

    def _draw(self, redraw=False):
        if redraw:
            wasp.system.bar.draw()
            
        now = wasp.system.bar.update()
        if not now:
            now = wasp.watch.rtc.get_localtime()

        draw = wasp.watch.drawable
        level = wasp.watch.battery.level();    

        h = now[3] % 12
        m = now[4]

        #draw.string('@'+str(self._mm)+':'+str(now[4]), self._HEIGHT_2,self._WIDTH_2)

        if redraw:
            for theta in range(12):
                c = self._C
                if theta == h:
                    c = self._D
                self._draw_h_at_in(draw, theta,c )

            for theta in range(12):
                c = self._C
                if m >= theta*5 and m < (theta+1)*5:
                    c = self._D

                self._draw_m_at_in(draw,theta,c)

            self._hh = h
            self._mm = m 
        else:
            if not now or self._mm == now[4]:
                # Skip the update
                #draw.string('s'+str(self._mm)+':'+str(now[4]), self._HEIGHT_2,self._WIDTH_2)
                return

        # Undraw old time
        for theta in range(12):
            if theta == self._hh:
                self._draw_h_at_in(draw, theta,self._C )
        for theta in range(12):
            if self._mm >= theta*5 and self._mm < (theta+1)*5:
                self._draw_m_at_in(draw,theta,self._C)

        # Draw the new time
        for theta in range(12):
            if theta == h:
                self._draw_h_at_in(draw,theta,self._D)

        for theta in range(12):
            if m >= theta*5 and m < (theta+1)*5:
                self._draw_m_at_in(draw,theta,self._D)

        # Record the minute that is currently being displayed
        self._hh = h
        self._mm = m

        #draw.string('r'+str(self._mm)+':'+str(now[4]), self._HEIGHT_2,self._WIDTH_2)

    def _draw_h_at_in(self,draw,h,c):
        for w in range(15):
            if w//2 == w/2:
                self._draw_polar( draw,self._HEIGHT_2,self._WIDTH_2, (h * 360 // 12)+w,  45,  53, 3,c )

    def _draw_m_at_in(self,draw,m,c):
        for w in range(24):
            # only every other - given width is 4px
            if w//2 == w/2:
                self._draw_polar( draw,self._HEIGHT_2,self._WIDTH_2, (m * 360 // 12)+w, 98, 112, 4,c )

    def _draw_polar(self,draw, x,y, th, r1,r2, w,c):
        draw.polar( x,y,th, r1,r2, w,c)
